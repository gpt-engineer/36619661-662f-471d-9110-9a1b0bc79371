// Fetch stock symbols from the API
fetch('https://financialmodelingprep.com/api/v3/stock/list')
    .then(response => response.json())
    .then(data => {
        const select = document.getElementById('stock-selector');
        data.forEach(stock => {
            const option = document.createElement('option');
            option.value = stock.symbol;
            option.text = `${stock.symbol} - ${stock.name}`;
            select.appendChild(option);
        });
    });

// Fetch stock data and generate chart when a stock symbol is selected
document.getElementById('stock-selector').addEventListener('change', function() {
    const symbol = this.value;
    fetch(`https://financialmodelingprep.com/api/v3/historical-price-full/${symbol}`)
        .then(response => response.json())
        .then(data => {
            const chartData = data.historical.map(day => {
                return { x: new Date(day.date), y: day.close };
            });
            const chartContainer = document.getElementById('chart-container');
            chartContainer.innerHTML = '';
            const chart = new CanvasJS.Chart(chartContainer, {
                title: { text: symbol },
                data: [{ type: 'line', dataPoints: chartData }]
            });
            chart.render();
        });
});
